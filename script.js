/**************************/
/***     POPUP CODE     ***/
/**************************/
const openPopupButtons = document.querySelectorAll('[data-popup-target]');
const closePopupButtons = document.querySelectorAll('[data-close-button]');
// const overlay = document.getElementById('overlay');

// loop over each open popup button
openPopupButtons.forEach(button => {
  // add event listener
  button.addEventListener('click', () => {
    // find popup button points to and open popup
    // (button.dataset.popupTarget) the css class is 'popup-target' but in js use camelCase
    const popup = document.querySelector(button.dataset.popupTarget);
    openPopup(popup);
  });
});
// loop over each close popup button
closePopupButtons.forEach(button => {
  button.addEventListener('click', () => {
    // we want to access the parent popup of the close button as it doesn't have a dataset
    // e.g. the closest parent that has class '.popup', then close
    const popup = button.closest('.popup');
    closePopup(popup);
  });
});
// Close any open popups by clicking on popup overlay
document.addEventListener('click', e => {
  let overlay = e.target;
  if (overlay.classList.contains('active')) {
    overlay.classList.remove('active');
  }
});

// Open popup
const openPopup = popup => {
  // check if a popup doesn't exist and exit
  if (popup == null) return;
  // add active classes to popup & overlay
  popup.classList.add('active');
  // overlay.classList.add('active');
};

// Close popup
const closePopup = popup => {
  // check if a popup doesn't exist and exit
  if (popup == null) return;
  // add active classes to popup & overlay
  popup.classList.remove('active');
  // overlay.classList.remove('active');
};

/***************************/
/* *** EVENT LISTENERS *** */
/***************************/
// SHOW POPUP ON PAGE LOAD
// SHOW POPUP AS SOON AS PAGE HAS LOADED
const showNoDelayPopup = () => {
  setTimeout(function () {
    console.log('No delay');

    // const popup = document.querySelectorAll('.popup')[3];
    // openPopup(popup);
  }, 0);
};
// WAIT 5 SECONDS THEN SHOW POPUP
const show5DelayPopup = () => {
  setTimeout(function () {
    console.log('5 second delay');
    const popup = document.querySelectorAll('.popup')[3];
    openPopup(popup);
  }, 5000);
};
window.addEventListener('load', function () {
  // run your open popup function after 5 sec = 5000
  showNoDelayPopup();
  show5DelayPopup();
});

// IF MOUSE GOES NEAR CLOSE BUTTON
const leavingPopup = () => {
  console.log("Don't Go!");
};
closePopupButtons.forEach(button => {
  button.addEventListener('mouseenter', leavingPopup);
});

// WILL BE ANNOYING!!!!
// const popupElement = document.querySelectorAll('[data-popup]');
// popupElement.forEach(popup => {
//   popup.addEventListener('mouseleave', leavingPopup);
// });

/* ********************** */
/* Get Button Id on Click */
/* ********************** */
const ctaBtns = document.querySelectorAll('.cta-btn');

const getBtnId = e => {
  console.log('btn id: ', e.target.getAttribute('id'));
};

ctaBtns.forEach(btn => btn.addEventListener('click', getBtnId));
